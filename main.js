window.onscroll = function() {myFunction()};

let about = document.getElementById("demo");

let sticky = about.offsetTop;

function myFunction() {
    if(window.pageYOffset >= sticky) {
        about.classList.add("sticky");
    } else {
        about.classList.remove("sticky");
    }
}

$(document).ready(function () {
    let courses = [];
    let courseList = [];
    let html = '';
    let html2 = '';

    $.get("http://demo6370041.mockable.io/getcourses", function (data, status) {
        courses =  data.data;
        console.log(courses);
        for (let i = 0;i<courses.length/4;i++) {
            courseList[i] = [
                courses[i*4],
                courses[i*4+1],
                courses[i*4+2],
                courses[i*4+3]
            ]
        }
        let html1 = '<p>Showing '+ 1 +' to ' + 4 + ' of '+ courses.length + ' entries';
        $('.left').html(html1);

        for (let i=0;i<courseList.length;i++) {
            if(i==0) {
                html2 += '<a class="btn-show btn-active" onclick="pageShow('+ i +')">'+(i+1)+'</a>';
            } else {
                html2 += '<a class="btn-show" onclick="pageShow('+ i +')">'+(i+1)+'</a>';
            }
        }

        $('.right-link').html(html2);

        for (let i=0;i<courseList[0].length;i++) {
            html+='<tr><td>'+courseList[0][i].id+'</td><td>'+courseList[0][i].name+'</td>';
            html+='</tr>'
        }

        console.log(html);
        $('.list-course').html(html);
    });


})

function pageShow(index) {

    let courses = [];
    let courseList = [];
    let html = '';
    let html2 = '';

    $.get("http://demo6370041.mockable.io/getcourses", function (data, status) {

        courses =  data.data;

        for (let i = 0;i<courses.length/4;i++) {
            courseList[i] = [
                courses[i*4],
                courses[i*4+1],
                courses[i*4+2],
                courses[i*4+3]
            ]
        }
        let html1 = '<p>Showing '+ 4*index+1 +' to ' + 4*index+4 + ' of '+ courses.length + ' entries';
        $('.left').html(html1);

        for (let i=0;i<courseList.length;i++) {
            if(i!=index) {
                html2 += '<a class="btn-show" onclick="pageShow('+ i +')">'+(i+1)+'</a>';
            } else {
                html2 += '<a class="btn-show btn-active" onclick="pageShow('+ i +')">'+(i+1)+'</a>';
            }
        }

        $('.right-link').html(html2);

        for (let i=0;i<courseList[index].length;i++) {
            html+='<tr><td>'+courseList[index][i].id+'</td><td>'+courseList[index][i].name+'</td>';
            html+='</tr>'
        }

        console.log(html);
        $('.list-course').html(html);
    });

}